from flask import Flask, jsonify
from faker import Faker
from faker.providers import person, profile

app = Flask(__name__)
fake = Faker()
fake.add_provider(person)
fake.add_provider(profile)
fake.seed_instance(4321)

BASE_ROUTE = '/api'
USER_COUNT = 10


def gen_user(id_):
    user = {}
    user['id'] = id_
    user['name'] = fake.name()
    user['address'] = fake.address()
    user['email'] = fake.email()
    user['job'] = fake.job()

    return user


@app.route(f'{BASE_ROUTE}/users', methods=['GET'])
def get_all_users():
    users = []
    for i in range(USER_COUNT):
        user = gen_user(i+1)
        users.append(user)
        print(user)

    return jsonify(users), 200


@app.route(f'{BASE_ROUTE}/users/<user_id>', methods=['GET'])
def get_user(user_id):
    user = gen_user(user_id)
    print('nothing')
    return jsonify(user), 200


if __name__ == '__main__':
    print('Starting application...')
    app.run(debug=True, host='0.0.0.0')
