FROM python:3.7-alpine

WORKDIR app

ADD requirements.txt .

RUN pip install -r requirements.txt

ADD mock_api mock_api/

RUN ls -R

CMD ["python", "mock_api/main.py"]