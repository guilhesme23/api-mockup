from tests.conftest import ASSERT_DATA

def test_get_users(client):
    result = client.get('/api/users')
    users = result.get_json()
    response_status = result.status

    assert isinstance(users, list)
    assert response_status == '200 OK'
    assert users == ASSERT_DATA

def test_get_user(client):
    result = client.get('/api/users/1')
    user = result.get_json()
    status = result.status

    assert_user = {
        "address": "47920 Allen Track\nWest Ruth, OR 64485",
        "email": "gallagherjoanne@clark.info",
        "id": "1",
        "job": "Estate manager/land agent",
        "name": "Carrie Todd"
    }

    print(user)

    assert isinstance(user, dict)
    assert status == '200 OK'
    assert user == assert_user
