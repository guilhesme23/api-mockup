import pytest
import json
from mock_api import main

with open('./tests/data.json', 'r') as f:
    ASSERT_DATA = json.load(f)

@pytest.fixture
def client():
    main.app.config['TESTING'] = True

    with main.app.test_client() as client:
        yield client